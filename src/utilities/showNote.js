export default function (fee, event, span) {

    event.stopImmediatePropagation();

    if (!fee.notes) return;

    let $tr = window.jQuery(event.target).parents('tr');

    if ($tr.hasClass('is-open')) {
        $tr.removeClass('is-open');
        $tr.next('.tf-infoNotes').remove();
    } else {
        $tr.addClass('is-open');

        let cls = ($tr.hasClass('odd')) ? 'tf-oddRow' : '';

        let $trNote = window.jQuery('<tr class="tf-infoNotes"><td colspan="' + span + '">' + fee.notes + '</td></tr>');
        $trNote.addClass(cls);
        $tr.after($trNote);

    }

}