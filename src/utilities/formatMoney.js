export default function (value) {

    if (value === null) {
        return 'N/A';
    }

    var formatter = new Intl.NumberFormat('en-US', {
        style: 'currency',
        currency: 'USD',
        minimumFractionDigits: 0
    });

    return formatter.format(parseInt(value));

}