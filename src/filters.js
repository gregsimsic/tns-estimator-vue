import Vue from 'vue'

Vue.filter('toCurrency', function (value) {
  // if (typeof value !== "number") {
  //   return value;
  // }
  if (value === null || value === 0) {
    return 'N/A';
  }
  const formatter = new Intl.NumberFormat('en-US', {
    style: 'currency',
    currency: 'USD',
    minimumFractionDigits: 0
  });
  return formatter.format(parseInt(value));
});

Vue.filter('capitalize', function (value) {
  if (!value) return '';
  value = value.toString();
  return value.charAt(0).toUpperCase() + value.slice(1);
});