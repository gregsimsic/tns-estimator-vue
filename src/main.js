import Vue from 'vue'
import VueRouter from 'vue-router'
import App from './components/App.vue'
import './filters'

Vue.use(VueRouter);

const router = new VueRouter();

Vue.config.productionTip = false; // suppresses console warning

document.addEventListener("DOMContentLoaded", function() {

  const mountEl = document.querySelector('#calc-app');

  new Vue({
    router,
    render: createElement => {
      const context = {
        props: { ...mountEl.dataset, dev: 'yes' },
      };
      return createElement(App, context);
    }
  }).$mount(mountEl);

});
