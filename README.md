# tns-tuition-app

Node 11

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).

### Change 'mode' in order to load an additional, overriding .env file
*NOTE: This is NOT working right now.*
```
npm run serve --mode development
```
This will load `.env` & `.env.development.local` (the 2nd overrides the 1st).

### How to Deploy

## Craft files: 
FTP via PHPStorm

## Database:
`dep db:push stage`

## Vue App Assets:
Run `npm run build` in the tns-tuition-app root

Copy app.css, app.js, and chunk-vendors.js from tns-tuition-app/dist to tns-craft/web/assets

FTP those assets to remote site
