module.exports = {
    filenameHashing: false,
    // to serve files while working on a project from a different server
    devServer: {
        disableHostCheck: true, // got rid of console errors
        hot: true,
        sockHost: 'http://localhost:8080',
        sockPort: 8080,
        headers: {
            'Access-Control-Allow-Origin': '*' // CORS
        }
    }


    // pluginOptions: {
    //     proxy: {
    //         enabled: true,
    //         context: [
    //             '/**',
    //             '!/dist/**'
    //         ],
    //         options: {
    //             target: 'http://tns-craft.lcl/'
    //         }
    //     }
    // },
    // devServer: {
    //     index: '', // specify to enable root proxying
    //     host: 'http://tns-craft.lcl/',
    //     // contentBase: '...',
    //     proxy: {
    //         context: () => true,
    //         target: 'http://tns-craft.lcl/',
    //     }
    // }
    // dev: {
    //     proxyTable: {
    //         // proxy all requests starting with /api to jsonplaceholder
    //         '/api': {
    //             target: 'http://jsonplaceholder.typicode.com',
    //             changeOrigin: true,
    //             pathRewrite: {
    //                 '^/api': ''
    //             }
    //         }
    //     }
    // }
}
